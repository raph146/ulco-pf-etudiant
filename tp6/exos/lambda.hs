main :: IO()
main = do
    print(map (\(c, i) -> c : '-' : show i) (zip['a'..'c'] [1..]))