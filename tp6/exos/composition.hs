plus42Positif :: Int -> Bool
plus42Positif = (>=) . (+42)

mul2PlusUn = (+1) . (2*)

mul2MoinsUn = (+(-1)) . (2*)

main :: IO()
main = do
    print(plus42Positif 2)
    print(plus42Positif -50)
    print(mul2MoinsUn 3)
    print(mul2PlusUn 3)