myTake2 :: [a] -> [a]
myTake2 = take 2

myGet2 :: [Int] -> Int
myGet2 = (flip (!!)) 2

main :: IO()
main = do
    print(map (*2) [1..4])
    print(map (/2) [1..4])