myRangeTuple1 :: (Int, Int) -> [Int]
myRangeTuple1 (a,b) = [a..b]

myRangeCurry1 :: Int -> Int -> [Int]
myRangeCurry1 a b = [a..b]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 b = myRangeTuple1 (0,b)

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 = myRangeCurry1 0

main :: IO()
main = do
    print(myRangeTuple1 (1,5))
    print(myRangeTuple2 8)
    print(myRangeCurry1 1 5)
    print(myRangeCurry2 8)