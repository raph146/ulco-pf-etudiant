import System.Random

main :: IO()
main = do
    target <- randomRIO (1, 100::Int)
    runGame target

runGame :: Int -> IO()
runGame target = do
    putStr "Type a number : "
    line <- getLine
    let guess = read line
    if guess < target
    then do
        putStr "Too small, try again"
        runGame target
    else
        if guess > target
        then do
            putStr "Too big, try again"
            runGame target
        else
            putStr "Congrats, you guessed"
