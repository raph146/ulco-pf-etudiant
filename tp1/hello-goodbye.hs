main :: IO()
main = do
    putStr "What's your name : "
    name <- getLine
    if name /= ""
    then do
        putStrLn ("Hello " ++ name)
        main
    else
        putStr ("Goodbye")