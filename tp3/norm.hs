dot :: [Double] -> [Double] -> Double
dot u v = sum (zipWith (*) x y)

main :: IO()
main = do
    print(dot [1,2,7] [3,4,5])