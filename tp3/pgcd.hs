pgcd :: Int -> Int -> Int
pgcd x y
    | y == 0 = x
    | otherwise = pgcd y (x `mod` y)

main :: IO()
main = do
    print(pgcd 49 35)
    print(pgcd 15 5)
    print(pgcd 7 0)
    print(pgcd 184 63)