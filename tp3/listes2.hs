mulListe :: Int -> [Int] -> [Int]
mulListe _ [] = []
mulListe x (n:ns) = x*n : mulListe x ns

selectListe :: (Int, Int) -> [Int] -> [Int]
selectListe _ [] = []
selectListe (a,b) (x:xs)
    | x>=a && x<=b = x : selectListe (a,b) xs
    | otherwise = selectListe (a,b) xs

sumListe :: Num a => [a] -> a
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs

main :: IO()
main = do
    print(mulListe 2 [1,2,3])
    print(mulListe 5 [1,2,3])
    print(selectListe (1,3) [0,2,3,5])
    print(sumListe [1.2, 2, 5])