fact :: Int -> Int
fact 1 = 1
fact n = n * fact (n-1)

factTco :: Int -> Int
factTco x = aux x 1
    where
        aux 1 acc = acc
        aux n acc = aux (n-1) (acc*n)

main :: IO()
main = do
    print(fact 4)
    print(fact 25)
    print(factTco 4)
    print(factTco 25)