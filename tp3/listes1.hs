import Data.Char

myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + (myLength xs)

myLength' :: [a] -> Int
myLength' l = aux l 0
    where   aux [] acc = acc
            aux (_:xs) acc = aux xs (acc+1)

toUpperString :: String -> String
toUpperString "" = ""
toUpperString (c:cs) = toUpper c : toUpperString cs

onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (c:cs)
    | isLetter c = c : onlyLetters cs
    | otherwise = onlyLetters cs

main :: IO()
main = do
    print(myLength [1,2,3,4,5])
    print(myLength' [1,2,3,4])
    print(toUpperString "bonjour")
    print(onlyLetters "hel lo")