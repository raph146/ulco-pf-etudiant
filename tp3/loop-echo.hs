loopEcho :: Int -> IO String
loopEcho 0 = return "Loop terminated"
loopEcho n = do
    str <- getLine
    if str /= ""
    then do
        putStrLn str
        loopEcho (n-1)
    else return "empty line"

main :: IO()
main = do
    result <- loopEcho 3
    putStrLn result