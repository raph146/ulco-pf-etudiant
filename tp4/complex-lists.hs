multiples :: Int -> [Int]
multiples n = [x | x <- [1..n], n `mod` x == 0]

--combinaisons :: [a] -> [b] -> [(a,b)]
--combinaisons u [] = u
--combinaisons [] v = v

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [(x,y,z) | x <- [1..n],
                            y <- [x..n],
                            z <- [1..n],
                            x^2 + y^2 == z^2]

main :: IO()
main = do
    print(multiples 35)
    print(tripletsPyth 13)