foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 u = foldl (+) 0 u

myFold :: (b -> a -> b) -> b -> [a] -> b
myFold _f acc [] = acc
myFold f acc (x:xs) = myFold f (f acc x) xs

main :: IO()
main = do
    print(foldSum1 [1..4])
    print(foldSum2 [1..4])