doubler :: [Int] -> [Int]
doubler l = [x*2 | x <- l]

pairs :: [Int] -> [Int]
pairs l = [x | x <- l, even x]

main :: IO()
main = do
    print(doubler [1..4])
    print(pairs [1..4])
