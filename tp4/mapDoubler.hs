doubler :: Int -> Int
doubler n = 2 * n

mapDoubler :: [Int] -> [Int]
mapDoubler [] = []
mapDoubler (n:ns) = doubler n : mapDoubler ns

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 u = map doubler u

main :: IO()
main = do
    print(mapDoubler [1,2,3,4])
    print(mapDoubler2 [1,2,3,4])