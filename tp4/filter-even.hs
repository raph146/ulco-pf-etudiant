filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs)
    | even x = x : filterEven1 xs
    | otherwise = filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 u = filter even u

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _p [] = []
myFilter p (x:xs)
    | p x = x : myFilter p xs
    | otherwise = myFilter p xs

main :: IO()
main = do
    print(filterEven1 [1..5])
    print(filterEven2 [1..5])