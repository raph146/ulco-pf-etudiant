fibo :: Int -> Int
fibo n =
    case n of
        0 -> 0
        1 -> 1
        _ -> fibo (n-1) + fibo (n-2)

main :: IO()
main = do
    print(fibo 0)
    print(fibo 1)
    print(fibo 2)
    print(fibo 3)
    print(fibo 4)
    print(fibo 5)