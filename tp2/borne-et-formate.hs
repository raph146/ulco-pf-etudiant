borneEtFormate1 :: Double -> String
borneEtFormate1 x = 
    show x ++ " -> " ++ show x'
    where x' = 
            if x < 0
            then 0
            else if x > 1
                then 1
                else x

borneEtFormate2 :: Double -> String
borneEtFormate2 x =
    show x ++ " -> " ++ show x'
    where x'    | x < 0 = 0
                | x > 1 = 1
                | otherwise = x

main :: IO()
main = do
    print(borneEtFormate1 0.3)
    print(borneEtFormate1 (-1))
    print(borneEtFormate1 3)

    print(borneEtFormate2 0.3)
    print(borneEtFormate2 (-1))
    print(borneEtFormate2 3)