formaterParite :: Int -> String
formaterParite x = 
    if even x
    then "pair"
    else "impair"

formaterSigne :: Int -> String
formaterSigne x =
    if x == 0
    then "nul"
    else if x < 0
        then "negatif"
        else "positif"

formaterPariteGarde :: Int -> String
formaterPariteGarde x
    | even x = "pair"
    | otherwise = "impair"

formaterSigneGarde :: Int -> String
formaterSigneGarde x
    | x == 0 = "nul"
    | x > 0 = "positif"
    | otherwise = "negatif"

main :: IO()
main = do
    putStr "Entrez un nombre : "
    line <- getLine
    let nb = read line
    print(formaterParite nb)
    print(formaterPariteGarde nb)