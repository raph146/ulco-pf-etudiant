import Text.Read (readMaybe)

main :: IO()
main = do
    str <- getLine
    let content = readMaybe str :: Maybe Int
    print content